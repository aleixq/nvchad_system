# nvchad_system

A wrapper to excellent nvchad to allow to use in a multiuser environment (such as server)

## Install

```bash
git clone https://gitlab.com/aleixq/nvchad_system  /etc/xdg/nvim --depth 1
cd /etc/xdg/nvim
git submodule init
git submodule update
git submodule update --remote
cd NvChad
git checkout -b v2.0 --track origin/v2.0
```


## Get customizations

`git clone https://gitlab.com/aleixq/nvchad_custom.git /etc/xdg/nvim/NvChad/lua/custom --depth 1`
